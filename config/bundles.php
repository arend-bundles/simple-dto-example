<?php

return [
    Symfony\Bundle\FrameworkBundle\FrameworkBundle::class => ['all' => true],
    ArendBundles\SimpleDTOBundle\ArendBundlesSimpleDTOBundle::class => ['all' => true],
];
