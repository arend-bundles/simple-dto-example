<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class TestCommand
 */
class TestCommand extends Command
{
    protected function configure(): void
    {
        $this->setName('cmd:test');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->write('Hello, world!'.PHP_EOL);

        return 0;
    }
}